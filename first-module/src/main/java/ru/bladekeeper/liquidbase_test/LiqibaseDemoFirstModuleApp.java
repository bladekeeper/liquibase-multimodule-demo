package ru.bladekeeper.liquidbase_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiqibaseDemoFirstModuleApp {

	public static void main(String[] args) {
		SpringApplication.run(LiqibaseDemoFirstModuleApp.class, args);
	}

}
